//
//  ViewController.m
//  recogMedicineAndTTS
//
//  Created by Hank  on 2015/1/27.
//  Copyright (c) 2015年 HwangDynChi. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *ocrImageView;
@property (weak, nonatomic) UIImage *ocrImage;
@property (weak, nonatomic) IBOutlet UITextView *recognizedTextView;
@end

@implementation ViewController

- (IBAction)startTTSbutton:(id)sender {
    if(0 == self.recognizedTextView.text.length) self.recognizedTextView.text = [NSString stringWithFormat:@"test"];
    //NSLog(@"%@", self.recognizedTextView.text);
    AVSpeechSynthesisVoice *voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"zh-TW"];
    //AVSpeechSynthesisVoice *voice = [AVSpeechSynthesisVoice voiceWithLanguage: CFBridgingRelease(CFStringTokenizerCopyBestStringLanguage((CFStringRef)self.textFieldForSpeech.text, CFRangeMake(0, self.textFieldForSpeech.text.length)))];
    AVSpeechSynthesizer *av = [[AVSpeechSynthesizer alloc] init];
    AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc]initWithString:self.recognizedTextView.text];
    utterance.voice = voice;
    [av speakUtterance:utterance];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.ocrImage = [UIImage imageNamed:@"sample_words.jpg"];
    [self.ocrImageView setImage: self.ocrImage];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startOCR:(id)sender {
    // self.recognizedTextView.text = NSLocalizedString(<#key#>, comment);
    self.recognizedTextView.text = @"辨識中...";
    [self.recognizedTextView setNeedsDisplay];
    
    NSDate *startTimeOCR = [NSDate date];
    Tesseract *tesseract = [[Tesseract alloc] initWithLanguage:@"chi_tra"];
    
    // tesseract.engineMode = OCREngineModeTesseractOnly;
    tesseract.delegate = self;  // self controller should implement shouldCancelImageRecognitionForTesseract
    
    // set particular limit chars
    // tesseract.charWhitelist = @"0123456";
    // tesseract.charBlackList = @"OoZx";
    
    tesseract.image = [self.ocrImage blackAndWhite];
    
    // ROI
    // tesseract.rect = CGRectMake(20, 20, 100, 100);
    
    // optional, set limit recognition time, it seems not support now
    // tesseract.maximumRecognitionTime = 2.0;
    
    [tesseract recognize];
    
    NSDate *endTimeOCR = [NSDate date];
    NSTimeInterval ocrTime = [endTimeOCR timeIntervalSinceDate:startTimeOCR];
    
    // Retrieve the recognized text
    self.recognizedTextView.text = [NSString stringWithFormat: @"OCR time: %.2f\n", ocrTime];
    self.recognizedTextView.text = [self.recognizedTextView.text stringByAppendingString:[tesseract recognizedText]];
}

- (void)progressImageRecognitionForTesseract:(Tesseract*)tesseract{
    self.recognizedTextView.text = [NSString stringWithFormat:@"progress: %lu", (unsigned long)tesseract.progress];
    NSLog(@"progress: %lu", (unsigned long)tesseract.progress);
}

@end
