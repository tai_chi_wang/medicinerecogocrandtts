//
//  ViewController.h
//  recogMedicineAndTTS
//
//  Created by Hank  on 2015/1/27.
//  Copyright (c) 2015年 HwangDynChi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TesseractOCR/TesseractOCR.h>

@interface ViewController : UIViewController <TesseractDelegate>


@end

